# Wat moet onze server kunnen?

Eerst beschrijven waarvoor we de server willen gebruiken, wat we graag zouden willen dat hij kan. We beschrijven hier niet welke software dan precies. De best passende software kan per functionaliteit gekozen worden. Daarover moeten we praten in issues.

> NOTE: We proberen een eigen box samen te stellen met daarop een eigen mengsel van software. We zoeken niet naar 'out-of-the-box' oplossingen als [Freenas](https://freenas.org) en [OpenMediaVault](https://openmediavault.org/). We moeten juist iets hebben dat modulair is zodat elk van ons zelf kan kiezen wat hij wel en niet gebruikt.

## Functionaliteiten

|Omschrijving | Zware server | Lichtgewicht |
|:--|:--:|:--:|
|Bestanden opslag en delen | 20TB | 500G |
|Eigen cloud (contacten/agenda/files) | :ballot_box_with_check: | |
|Mediaserver | :ballot_box_with_check: | :ballot_box_with_check: |
|SSH loginserver | :ballot_box_with_check: | :ballot_box_with_check: |
|Internetrouter/Gateway/Proxy | :ballot_box_with_check: | :ballot_box_with_check: |
|Wi-Fi Access point | :ballot_box_with_check: | |
|Mailserver | :ballot_box_with_check: | |
|Monitoring | :ballot_box_with_check: | |
|Grafieken | :ballot_box_with_check: | |
|Home automation | :ballot_box_with_check: | :ballot_box_with_check: |
|Out-of-band management | :ballot_box_with_check: | |
|Webserver | :ballot_box_with_check: | :ballot_box_with_check: |
|Virtualisatie en/of containers | :ballot_box_with_check: | |
|Backup | :ballot_box_with_check: | |
|Repo cache | :ballot_box_with_check: | |
|Storage moet encrypted zijn | :ballot_box_with_check: | :ballot_box_with_check: |
|Downloadsoftware (news en torrent downloader) | :ballot_box_with_check: | :ballot_box_with_check: |
|DHCP/DNS | :ballot_box_with_check: | :ballot_box_with_check: |
|NTP | :ballot_box_with_check: | :ballot_box_with_check: |
|PXE installatie server | :ballot_box_with_check: | |
|Een of enkele database servers | :ballot_box_with_check: | |
|Matrix Synapse service | :ballot_box_with_check: | :ballot_box_with_check: |
|UPS | :ballot_box_with_check: | :ballot_box_with_check: |
|Energiezuinig | :ballot_box_with_check: | :ballot_box_with_check: |
|Stil | :ballot_box_with_check: | :ballot_box_with_check: |
|2 NIC's tbv router (één glas?) | :ballot_box_with_check: | |
|Disks moeten allemaal redundant zijn (RAID/ZFS) | :ballot_box_with_check: | |
|Genoeg mem tbv virtualisatie en containers. | :ballot_box_with_check: | |
